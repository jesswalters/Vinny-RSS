package com.rss.utils

enum class RssProvider(val value: Int) {
    REDDIT(1), TWITTER(2), OTHER(3)
}